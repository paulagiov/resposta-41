/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Paula G Rodrigues
 */
public class Elipse {
    public double semieixoMaior, semieixoMenor;
    
    public Elipse(){
        
    }
    
    public Elipse(double semieixoMaior, double semieixoMenor){
        this.semieixoMaior = semieixoMaior;
        this.semieixoMenor = semieixoMenor;
    }
    
    public double getArea(){
        double a = Math.PI*semieixoMaior*semieixoMenor;
        return a;
    }
    
    public double getPerimetro(){
        double p = Math.PI * (3 * (semieixoMaior + semieixoMenor) - Math.sqrt((3*semieixoMaior + semieixoMenor)*(semieixoMaior + 3*semieixoMenor)));
        return p;
    }
}
